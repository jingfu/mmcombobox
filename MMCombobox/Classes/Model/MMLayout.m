//
//  MMLayout.m
//  MMComboBoxDemo
//
//  Created by wyy on 2016/12/15.
//  Copyright © 2016年 wyy. All rights reserved.
//

#import "MMLayout.h"
#import "MMItem.h"
#import "NSArray+Safe.h"

@interface MMLayout ()

@end

@implementation MMLayout

- (instancetype)init {
    self = [super init];
    if (self) {
        
        self.cellLayoutTotalHeight = [NSMutableArray array];
        self.cellLayoutTotalInfo = [NSMutableArray array];
        self.cellCloseStateHeight = [NSMutableArray array];
        [self _initUI];
    }
    return self;
}

- (void)_initUI {
    
}

+ (instancetype)layoutWithItem:(MMItem *)item {
    MMLayout *layout = [[MMLayout alloc] init];
    layout.headerViewHeight = item.alternativeArray.count * (2*AlternativeTitleVerticalMargin + AlternativeTitleHeight);
    layout.totalHeight += layout.headerViewHeight;
    for (int i = 0; i < item.childrenNodes.count; i++) {
        MMItem *subItem = [item.childrenNodes safeObjectAtIndex:i];
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:subItem.childrenNodes.count];
        CGFloat totalCellHeight = 2*TitleVerticalMargin + ItemHeight;
        
        
        CGFloat  fOriginX = ItemHorizontalMargin;
        CGFloat  fOriginY = (2*TitleVerticalMargin + ItemHeight);
        CGFloat  fCurrentX = fOriginX;
        CGFloat  fCurrentY = fOriginY;
        int currentLevel = 0;
        for (int j = 0; j < subItem.childrenNodes.count; j++) {
            
            NSMutableArray *subArray = [NSMutableArray arrayWithCapacity:5];
            MMItem *subsubItem = [subItem.childrenNodes safeObjectAtIndex:j];
            CGFloat buttonWidth = [MMLayout layoutItemWidth:subsubItem];
            if (fCurrentX > [MMLayout layoutTotalWidth])  {
                fCurrentX = fOriginX;
                fCurrentY += ItemHeight+TitleVerticalMargin;
                currentLevel++;
            }
            [subArray safeAddObject:@(fCurrentX)];
            [subArray safeAddObject:@(fCurrentY)];
            if (buttonWidth > [MMLayout layoutTotalWidth]) {
                buttonWidth = [MMLayout layoutTotalWidth];
            }
            [subArray safeAddObject:@(buttonWidth)];
            [subArray safeAddObject:@(ItemHeight)];
            [subArray safeAddObject:@(currentLevel)];
            [array safeAddObject:subArray];
            fCurrentX += (buttonWidth+ItemHorizontalDistance);
        }
        
        NSInteger columnNumber = MAX(1,currentLevel+1);
        
        CGFloat closeStateHeight = 2*TitleVerticalMargin + ItemHeight;
        closeStateHeight += MIN(1, columnNumber)*(ItemHeight + TitleVerticalMargin); //(默认显示一行的代码)
        
        totalCellHeight += columnNumber*(ItemHeight + TitleVerticalMargin);
        
        [layout.cellLayoutTotalHeight addObject:@(totalCellHeight)];
        [layout.cellCloseStateHeight addObject:@(closeStateHeight)];
        
        layout.totalHeight += totalCellHeight;
        [layout.cellLayoutTotalInfo addObject:array];
    }
    return  layout;
}


+ (CGFloat)layoutTotalWidth
{
    return kMMScreenWidth-60;
}
+ (CGFloat)layoutItemWidth:(MMItem*)item
{

   // item.title = @"我就不掉你你能够咋样我就不掉你你能够咋样我就不掉你你能够咋样我就不掉你你能够咋样";
    CGSize size = [item.title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:ButtonFontSize]}];
    return size.width+20;
}
+ (CGFloat)layoutItemHeight:(MMItem*)item
{
    return ItemHeight;
}


@end
