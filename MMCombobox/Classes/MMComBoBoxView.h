//
//  MMComBoBoxView.h
//  MMComboBoxDemo
//
//  Created by wyy on 2016/12/7.
//  Copyright © 2016年 wyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMItem.h"

typedef NS_ENUM(NSUInteger, MMComBoBoxViewShowActionType) {  //分辨弹出框的动作
    MMComBoBoxViewShowActionTypePop = 0,                //弹出弹出框
    MMComBoBoxViewShowActionTypePackUp = 1,            //收起弹出框
};

@protocol MMComBoBoxViewDataSource;
@protocol MMComBoBoxViewDelegate;

@interface MMComBoBoxView : UIView
@property (nonatomic,weak,nullable) id<MMComBoBoxViewDataSource> dataSource;
@property (nonatomic,weak,nullable) id<MMComBoBoxViewDelegate> delegate;
@property (nonatomic,assign,readonly) BOOL isPopviewShow;//(popView 是否在显示)
@property (nonatomic,assign)BOOL showCornerAndShadows;//(是否显示背景圆角和影院，默认显示，如不显示，初始化之后该属性请置为NO)
@property (nonatomic,assign)CGFloat leftSpaceMargin;//左边留白距离，默认10
@property (nonatomic,assign)CGFloat rightSpaceMargin;//右边边留白距离，默认10

/**
 根据boxValues 设置当前筛选项的title，以及当前选中的值
 
 @param boxValues MMComBoxOldValue
 */
- (void)updateValueWithData:(nullable NSArray <MMComBoxOldValue>*)boxValues;

/**
 清除所有当前的选择
 */
- (void)cleanAllChoice;

/**
 重载数据
 */
- (void)reload;

/**
 收起下拉框
 */
- (void)dimissPopView;

/**
 初始化MMComBoBoxView
 
 @param frame frame
 @param spaceMargin 上下留白的距离`
 @return MMComBoBoxView
 */
- (nullable id)initWithFrame:(CGRect)frame spaceToTop:(CGFloat)spaceMargin;


/**
 初始化MMComBoBoxView
 
 @param frame frame
 @param topSpaceMargin 上方留白距离
 @param bottomSpaceMargin 下方留白距离
 @return MMComBoBoxView
 */
- (nullable id)initWithFrame:(CGRect)frame topSpace:(CGFloat)topSpaceMargin bottomSpace:(CGFloat)bottomSpaceMargin;

/**
 初始化MMComBoBoxView
 
 @param frame frame
 @param topSpaceMargin 上方留白距离
 @param bottomSpaceMargin 下方留白距离
 @param showCornerAndShadows 是否显示阴影和圆角
 @return MMComBoBoxView
 */
- (nullable id)initWithFrame:(CGRect)frame topSpace:(CGFloat)topSpaceMargin bottomSpace:(CGFloat)bottomSpaceMargin showCornerAndShadows:(BOOL)showCornerAndShadows;


@end

@protocol MMComBoBoxViewDataSource <NSObject>

@required;

/**
 有几列数据
 
 @param comBoBoxView MMComBoBoxView
 @return 列数
 */
- (NSUInteger)numberOfColumnsIncomBoBoxView :(nullable MMComBoBoxView *)comBoBoxView;

/**
 每一列的第一行rootItem
 
 @param comBoBoxView MMComBoBoxView
 @param column 列
 @return rootItem（MMItem）
 */
- (nullable MMItem *)comBoBoxView:(nullable MMComBoBoxView *)comBoBoxView infomationForColumn:(NSUInteger)column;
@end

@protocol MMComBoBoxViewDelegate <NSObject>

@optional

/**
 用户选择或者确定了某一数据
 
 @param comBoBoxViewd MMComBoBoxView
 @param array 保存选中的MMSelectPath选项
 @param index MMDropDownBox的index（当前选择的index）
 */
- (void)comBoBoxView:(nullable MMComBoBoxView *)comBoBoxViewd didSelectedItemsPackagingInArray:(nullable NSArray *)array atIndex:(NSUInteger)index;

/**
 用户弹出下拉框或者收起下拉框的消息
 
 @param comBoBoxViewd MMComBoBoxView
 @param action 弹出框是弹出还是收起
 @param index MMDropDownBox的index（当前选择收起或者弹出的index）
 */
- (void)comBoBoxView:(nullable MMComBoBoxView *)comBoBoxViewd actionType:(MMComBoBoxViewShowActionType)action atIndex:(NSUInteger)index;


/**
 用户选择或者确定了某一数据
 
 @param comBoBoxViewd MMComBoBoxView
 @param array 保存选中的MMItem选项
 @param index MMDropDownBox的index（当前选择的index）
 */
- (void)comBoBoxView:(nullable MMComBoBoxView *)comBoBoxViewd didSelectedModelsInArray:(nullable NSArray *)array atIndex:(NSUInteger)index;


/**
 点击重置按钮的回调
 
 @param popupView rootItem
 @param index tag
 */
- (void)popupViewDidResetValue:(nullable MMItem *)rootItem atIndex:(NSUInteger)index;


@end
