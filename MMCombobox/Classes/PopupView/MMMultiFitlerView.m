//
//  MMMultiFitlerView.m
//  MMComboBoxDemo
//
//  Created by wyy on 2016/12/8.
//  Copyright © 2016年 wyy. All rights reserved.
//

#import "MMMultiFitlerView.h"
#import "MMHeader.h"
#import "MMLeftCell.h"
#import "MMNormalCell.h"
#import "MMSelectedPath.h"
#import "NSMutableArray+Safe.h"

@interface MMMultiFitlerView () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, assign) NSUInteger minRowNumber;

@end

@implementation MMMultiFitlerView

- (id)initWithItem:(MMItem *)item{
    self = [super init];
    if (self) {
        self.item = item;
        [self updateSelectPath];
        self.minRowNumber = kMMMinShowRowNumer;
        self.backgroundColor = kMMPopViewBackGroundColor;
    }
    return self;
}

-(void)updateSelectPath
{
    [self.selectedArray removeAllObjects];
    NSArray  *array = [self _findAllSelectItem];
    for (MMItem  *item in array) {
        NSUInteger index = [self.item.childrenNodes indexOfObject:item.parentItem];
        NSUInteger secondIndex = [item.parentItem.childrenNodes indexOfObject:item];
        MMSelectedPath *selectedPath = [MMSelectedPath pathWithFirstPath:index secondPath:secondIndex];
        if (index < self.item.childrenNodes.count && secondIndex < item.parentItem.childrenNodes.count) {
            if (self.selectedArray.count > 0) {
                MMItem  *subItem = [self.item findItemBySelectedPath:selectedPath];
                subItem.isSelected = NO;
                item.isSelected = NO;
            }else{
                
                MMItem  *subItem = [self.item findItemBySelectedPath:selectedPath];
                subItem.isSelected = YES;
                item.isSelected = YES;
                [self.selectedArray safeAddObject:selectedPath];
            }
            
        }
       
    }
 
    if (self.selectedArray.count < 1) {
        self.selectedIndex = 0;
        if (self.item.childrenNodes.count > 0) {
            
             MMSelectedPath *firstPath = [MMSelectedPath pathWithFirstPath:0];
             MMItem *firstItem = [self.item findItemBySelectedPath:firstPath];
            if (firstItem.childrenNodes.count > 0) {
                MMSelectedPath *selectedPath = [MMSelectedPath pathWithFirstPath:0 secondPath:0];
                self.selectedIndex = 0;
                MMItem *selectItem = [self.item findItemBySelectedPath:selectedPath];
                selectItem.isSelected = YES;
                firstItem.isSelected = YES;
                [self.selectedArray safeAddObject:selectedPath];
            }
        }
       
    }
    
    self.selectedIndex = [self _findLeftSelectedIndex];
    [self resetSelectIndex];
}

- (void)resetSelectIndex
{
    if (self.selectedArray.count > 0) {
        MMSelectedPath *path = [self.selectedArray safeObjectAtIndex:0];
        MMItem *tempItem = [self.item findItemBySelectedPath:path];
        tempItem.parentItem.isSelected = YES;
        tempItem.isSelected = YES;
        NSUInteger index = [self.item.childrenNodes indexOfObject:tempItem.parentItem];
        if (index < self.item.childrenNodes.count) {
            self.selectedIndex = index;
        }else{
            self.selectedIndex = 0;
        };
    }else{
        self.selectedIndex = 0;
    }
}


#pragma mark - public method

- (void)popupViewFromView:(nullable UIView*)superView completion:(void (^ __nullable)(void))completion{

    CGFloat top =  CGRectGetHeight(superView.frame);
    CGFloat maxHeight = kMMScreenHeigth - DistanceBeteewnPopupViewAndBottom - top - PopupViewTabBarHeight-DistanceBeteewnTopMargin;
    
    NSInteger maxCount = self.item.childrenNodes.count;
    for (MMItem *item in self.item.childrenNodes) {
        maxCount = MAX(item.childrenNodes.count, maxCount);
    }
    CGFloat resultHeight = MIN(maxHeight, MAX(maxCount, self.minRowNumber)  * [MMNormalCell normalCellHeight:nil]);
    resultHeight = MIN(resultHeight, MAX(self.item.childrenNodes.count, self.minRowNumber)  * [MMLeftCell leftCellHeight:nil]);
    if (self.item.childrenNodes.count == 1) {
        resultHeight = self.minRowNumber * [MMNormalCell normalCellHeight:nil];
    }
    self.frame = CGRectMake(10, top/2, kMMScreenWidth-20, 0);
    [superView insertSubview:self atIndex:0];
    
    
    UIView *halfBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMMScreenWidth-20, 0)];
    [halfBackView setBackgroundColor:kMMPopViewBackGroundColor];
  
  
    //add tableView
    self.mainTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, top/2, kMMLeftCellWidth, 0) style:UITableViewStylePlain];
    if (self.minRowNumber > self.item.childrenNodes.count) {
        self.mainTableView.rowHeight = [MMLeftCell leftCellHeight:nil]*self.minRowNumber/self.item.childrenNodes.count;
    }else{
        self.mainTableView.rowHeight =  [MMLeftCell leftCellHeight:nil];
    }
    
    self.mainTableView.tag = 0;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.showsVerticalScrollIndicator = YES;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.separatorColor = [UIColor clearColor];
    self.mainTableView.layer.masksToBounds = YES;
    self.mainTableView.layer.cornerRadius = 5;
    [self.mainTableView registerClass:[MMLeftCell class] forCellReuseIdentifier:MainCellID];
    [self addSubview:self.mainTableView];
    self.mainTableView.backgroundColor = [UIColor ff_colorWithHex:0xe6e6e6];
    
    self.subTableView = [[UITableView alloc] initWithFrame:CGRectMake(kMMLeftCellWidth+10, 0,  self.ff_width - kMMLeftCellWidth-20, 0) style:UITableViewStylePlain];
    self.subTableView.rowHeight = [MMNormalCell normalCellHeight:nil];
    self.subTableView.tag = 1;
    self.subTableView.delegate = self;
    self.subTableView.dataSource = self;
    self.subTableView.layer.masksToBounds = YES;
    self.subTableView.layer.cornerRadius = 5;
    self.subTableView.backgroundColor = kMMPopViewBackGroundColor;
    self.subTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.subTableView registerClass:[MMNormalCell class] forCellReuseIdentifier:SubCellID];
    [self addSubview:self.subTableView];
    
    
    //add ShadowView
    self.shadowView.frame = CGRectMake(0, 0, kMMScreenWidth, kMMScreenHeigth);
    self.shadowView.alpha = 0;
    self.shadowView.userInteractionEnabled = YES;
    [superView insertSubview:self.shadowView belowSubview:self];
    [self.shadowView addTarget:self action:@selector(emptyAction:) forControlEvents:UIControlEventAllEvents];
    [self.shadowView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(emptyAction:)]];
    
    
    UIView *leftCoverView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.mainTableView.frame)-10, CGRectGetMaxY(self.mainTableView.frame)-5, 10, 10)];
    [leftCoverView setBackgroundColor:self.mainTableView.backgroundColor];
    
    UIView *rightCoverView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.subTableView.frame), CGRectGetMaxY(self.subTableView.frame)-5, 10, 10)];
    [rightCoverView setBackgroundColor:self.subTableView.backgroundColor];
    [self insertSubview:leftCoverView atIndex:0];
    [self insertSubview:rightCoverView atIndex:0];
    [self insertSubview:halfBackView atIndex:0];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 5;
    //出现的动画
    [UIView animateWithDuration:AnimationDuration animations:^{
        
        self.frame = CGRectMake(10, top/2, kMMScreenWidth-20, resultHeight+top/2);
        self.mainTableView.frame = CGRectMake(0, top/2, kMMLeftCellWidth, self.ff_height-top/2);
        self.subTableView.frame = CGRectMake(kMMLeftCellWidth+10, top/2,  kMMScreenWidth-20-kMMLeftCellWidth-10, self.ff_height-top/2);
        self.shadowView.alpha = ShadowAlpha;
        [leftCoverView setFrame:CGRectMake(CGRectGetMaxX(self.mainTableView.frame)-10, CGRectGetMaxY(self.mainTableView.frame)-5, 10, 5)];
        [rightCoverView setFrame:CGRectMake(CGRectGetMinX(self.subTableView.frame), CGRectGetMaxY(self.subTableView.frame)-5, 10, 5)];
        [halfBackView setFrame:CGRectMake(0, 0, kMMScreenWidth-20, top)];
    
    } completion:^(BOOL finished) {
        if (completion) {
            completion();
        }
    }];
    
}

/**
 只是为了滑动事件不被父view接受
 
 @param sender sender
 */
-(void)emptyAction:(id)sender
{
    if (!self.shadowView.superview) {
        return;
    }
      [self dismissWithCompletion:nil];
}

- (void)dismissWithCompletion:(void (^)(void))completion{
    [super dismissWithCompletion:completion];
    //设置最后选中的赋给left cell
    MMSelectedPath *path = [self.selectedArray lastObject];
    if (path) {
        NSUInteger index = [self _findLeftSelectedIndex];
        if (index != path.firstPath) {
            [[self.item.childrenNodes safeObjectAtIndex:index] setIsSelected:NO];
            [[self.item.childrenNodes safeObjectAtIndex:path.firstPath] setIsSelected:YES];
        }
    }
   
    
    if ([self.delegate respondsToSelector:@selector(popupViewWillDismiss:)]) {
        [self.delegate popupViewWillDismiss:self];
    }
    //消失的动画
    [UIView animateWithDuration:AnimationDuration animations:^{
        self.mainTableView.ff_height = 0;
        self.subTableView.ff_height = 0;
        self.ff_height = 0;
    } completion:^(BOOL finished) {
        if (self.superview) {
            [self removeFromSuperview];
        }
        if (completion) {
            completion();
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(popupViewDidDismiss:)]) {
            [self.delegate popupViewDidDismiss:self];
        }
    }];
}

#pragma mark - private method

- (NSArray*)_findAllSelectItem {
    
    NSMutableArray  *items = [NSMutableArray array];
    for (MMItem *item in self.item.childrenNodes) {
        
        for (MMItem *subItem in item.childrenNodes) {
            if (subItem.isSelected)
            {
                [items safeAddObject:subItem];
            }
        }
       
    }
    return [NSArray arrayWithArray:items];
}
- (NSUInteger)_findLeftSelectedIndex {
    for (MMItem *item in self.item.childrenNodes) {
        if (item.isSelected) return [self.item.childrenNodes indexOfObject:item];
    }
    return NSUIntegerMax;
}

- (NSInteger)_findRightSelectedIndex:(NSInteger)leftIndex {
    MMItem *item = self.item.childrenNodes[leftIndex];
    for (MMItem *subItem in item.childrenNodes) {
        if (subItem.isSelected) return [item.childrenNodes indexOfObject:subItem];
    }
    return -1;
}

- (void)_callBackDelegate {
    if ([self.delegate respondsToSelector:@selector(popupView:didSelectedItemsPackagingInArray:atIndex:)]) {
        [self.delegate popupView:self didSelectedItemsPackagingInArray:self.selectedArray  atIndex:self.tag];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissWithCompletion:nil];
        });
    }
}

#pragma mark - Action
- (void)respondsToTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    [self dismissWithCompletion:nil];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 0) { //mainTableView
       return self.item.childrenNodes.count;
    }
    return self.item.childrenNodes[self.selectedIndex].childrenNodes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 0) { //mainTableView
        MMLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:MainCellID forIndexPath:indexPath];
        cell.item = self.item.childrenNodes[indexPath.row];
        return cell;
    }
    MMNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:SubCellID forIndexPath:indexPath];
    cell.item = self.item.childrenNodes[self.selectedIndex].childrenNodes[indexPath.row];
    return cell;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        return [MMLeftCell leftCellHeight:self.item.childrenNodes[indexPath.row]];
    }
    return [MMNormalCell normalCellHeight:self.item.childrenNodes[self.selectedIndex].childrenNodes[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 0) { //mainTableView
        
        if (self.selectedIndex == indexPath.row) return;
        self.item.childrenNodes[indexPath.row].isSelected = YES;
        self.item.childrenNodes[self.selectedIndex].isSelected = NO;
        self.selectedIndex = indexPath.row;
        [self.mainTableView reloadData];
        [self.subTableView reloadData];
    }else{ //subTableView
        
        MMSelectedPath *selectdPath = [self.selectedArray lastObject];
        if (selectdPath.firstPath == self.selectedIndex && selectdPath.secondPath == indexPath.row)
        {
            [self _callBackDelegate];
            return;
        }
        if (selectdPath.secondPath != -1) {
            MMItem *lastItem = self.item.childrenNodes[selectdPath.firstPath].childrenNodes[selectdPath.secondPath];
            lastItem.isSelected = NO;
        }
        [self removeAndEmptySelected];
        [self.selectedArray removeAllObjects];
        MMItem *currentIndex = self.item.childrenNodes[self.selectedIndex].childrenNodes[indexPath.row];
        currentIndex.isSelected = YES;
        currentIndex.parentItem.isSelected = YES;
        [self.selectedArray addObject:[MMSelectedPath pathWithFirstPath:self.selectedIndex secondPath:indexPath.row]];
        [self.subTableView reloadData];
        [self.mainTableView reloadData];
        [self _callBackDelegate];
    }
}

#pragma mark
-(void)removeAndEmptySelected
{
    [self.item setAllSubItemSelected:NO];
}
@end
