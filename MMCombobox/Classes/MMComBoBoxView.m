//
//  MMComBoBoxView.m
//  MMComboBoxDemo
//
//  Created by wyy on 2016/12/7.
//  Copyright © 2016年 wyy. All rights reserved.
//

#import "MMComBoBoxView.h"
#import "MMDropDownBox.h"
#import "MMHeader.h"
#import "MMBasePopupView.h"
#import "MMSelectedPath.h"

@interface MMComBoBoxView () <MMDropDownBoxDelegate,MMPopupViewDelegate>

@property (nullable, nonatomic, strong) NSMutableArray *dropDownBoxArray;
@property (nullable, nonatomic, strong) NSMutableArray *itemArray;
@property (nullable, nonatomic, strong) NSMutableArray *symbolArray;  //当成一个队列来标记那个弹出视图
@property (nullable, nonatomic, strong) MMBasePopupView *popupView;
@property (nullable, nonatomic, strong) UIView          *topBgView;
@property (nonatomic, assign) CGFloat         topSpaceMargin;
@property (nonatomic, assign) CGFloat         bottomSpaceMargin;
@property (nonatomic, assign) BOOL            isAnimation;
@property (nullable, nonatomic, strong) MMItem           *oldDistriItem;

@end

@implementation MMComBoBoxView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.showCornerAndShadows = YES;
        self.leftSpaceMargin = 10;
        self.rightSpaceMargin = 10;
        [self initize];
    }
    return self;
}

- (nullable id)initWithFrame:(CGRect)frame spaceToTop:(CGFloat)spaceMargin{
    self = [super initWithFrame:frame];
    if (self) {
        self.topSpaceMargin = spaceMargin;
        self.bottomSpaceMargin = spaceMargin;
        self.showCornerAndShadows = YES;
        self.leftSpaceMargin = 10;
        self.rightSpaceMargin = 10;
        [self initize];
    }
    return self;
}


/**
 初始化MMComBoBoxView
 
 @param frame frame
 @param topSpaceMargin 上方留白距离
 @param bottomSpaceMargin 下方留白距离
 @return MMComBoBoxView
 */
- (nullable id)initWithFrame:(CGRect)frame topSpace:(CGFloat)topSpaceMargin bottomSpace:(CGFloat)bottomSpaceMargin{
    self = [super initWithFrame:frame];
    if (self) {
        self.topSpaceMargin = topSpaceMargin;
        self.bottomSpaceMargin = bottomSpaceMargin;
        self.showCornerAndShadows = YES;
        self.leftSpaceMargin = 10;
        self.rightSpaceMargin = 10;
        [self initize];
    }
    return self;
}



/**
 初始化MMComBoBoxView
 
 @param frame frame
 @param topSpaceMargin 上方留白距离
 @param bottomSpaceMargin 下方留白距离
 @param showCornerAndShadows 是否显示阴影和圆角
 @return MMComBoBoxView
 */
- (nullable id)initWithFrame:(CGRect)frame topSpace:(CGFloat)topSpaceMargin bottomSpace:(CGFloat)bottomSpaceMargin showCornerAndShadows:(BOOL)showCornerAndShadows{
    self = [super initWithFrame:frame];
    if (self) {
        self.topSpaceMargin = topSpaceMargin;
        self.bottomSpaceMargin = bottomSpaceMargin;
        self.showCornerAndShadows = showCornerAndShadows;
        self.leftSpaceMargin = 10;
        self.rightSpaceMargin = 10;
        [self initize];
    }
    return self;
}

- (void)initize
{
    self.dropDownBoxArray = [NSMutableArray array];
    self.itemArray = [NSMutableArray array];
    self.symbolArray = [NSMutableArray arrayWithCapacity:1];
    self.backgroundColor =  [UIColor ff_colorWithHex:0xf6f6f6];//[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1];
    [self addSubview:self.topBgView];
    [self.topBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftSpaceMargin);
        make.right.mas_offset(-self.rightSpaceMargin);
        make.top.mas_equalTo(self.topSpaceMargin);
        make.bottom.mas_offset(-self.bottomSpaceMargin);
    }];
    
}

- (void)setLeftSpaceMargin:(CGFloat)leftSpaceMargin
{
    _leftSpaceMargin = leftSpaceMargin;
    [self setNeedsDisplay];
}

- (void)setRightSpaceMargin:(CGFloat)rightSpaceMargin
{
    _rightSpaceMargin = rightSpaceMargin;
    [self setNeedsDisplay];
}

- (void)setShowCornerAndShadows:(BOOL)showCornerAndShadows
{
    _showCornerAndShadows = showCornerAndShadows;
    [self setNeedsDisplay];
}


-(void)emptyAction:(id)sender
{
    // [self dismissWithCompletion:nil];
}



-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.topBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftSpaceMargin);
        make.right.mas_offset(-self.rightSpaceMargin);
        make.top.mas_equalTo(self.topSpaceMargin);
        make.bottom.mas_offset(-self.bottomSpaceMargin);
    }];
    if (self.showCornerAndShadows) {
        self.topBgView.layer.cornerRadius = self.topBgView.ff_height/2;
        self.topBgView.layer.shadowRadius = 3;
        self.topBgView.layer.shadowOffset = CGSizeMake(0, 3);
    }else{
        self.topBgView.layer.cornerRadius = 0;
        self.topBgView.layer.shadowRadius = 0;
        self.topBgView.layer.shadowOffset = CGSizeZero;
    }
    
    
}

- (nullable UIView*)topBgView
{
    if (_topBgView == nil) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.layer.shadowColor = [UIColor ff_colorWithHex:0xcccccc].CGColor;
        bgView.layer.shadowOffset = CGSizeMake(0, 3);
        bgView.layer.shadowOpacity = 0.5;
        bgView.backgroundColor = [UIColor clearColor];
        bgView.backgroundColor = [UIColor whiteColor];
        [bgView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(emptyAction:)]];
        _topBgView = bgView;
    }
    
    return _topBgView;
}


/**
 根据boxValues获取rootItem 中对应的值

 @param rootItem rootItem
 @param boxValues 选中值
 @return  NSMutableDictionary items对应选中的MMItem，boxs对应选中items对应的选中值
 */
-(NSMutableDictionary*)getSelectItemBoxValue:(nullable MMItem*)rootItem boxArray:(nullable NSArray *)boxValues
{
    NSMutableArray *returnItems  = [NSMutableArray arrayWithCapacity:2];
    FFBaseKeyCodeModel *returnModel = nil;
    NSMutableArray *returnModels = [NSMutableArray arrayWithCapacity:2];
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionaryWithCapacity:2];
    
    for (FFBaseKeyCodeModel *box in boxValues) {
        
        for (MMItem *subItem in rootItem.childrenNodes) {
            
            if (subItem.childrenNodes.count <= 0) {
                
                if (([subItem.key isEqualToString:[box key]] && subItem.key2 == nil)
                    || ([subItem.key2 isEqualToString:[box key2]] && [subItem.key isEqualToString:[box key]])) {
                    NSArray *codeArray = [[box code] componentsSeparatedByString:@","];
                    NSArray *code2Array = [[box code2] componentsSeparatedByString:@","];
                    for (NSString *code in codeArray) {
                        if ([code isEqualToString:subItem.code]) {
                            if (code2Array.count > 0) {
                                
                                for (NSString *code2 in code2Array)
                                {
                                    if ([code2 isEqualToString:subItem.code2])
                                    {
                                        subItem.isSelected = YES;
                                        subItem.parentItem.isSelected = YES;
                                        [returnItems safeAddObject:subItem];
                                        if (returnModel == nil) {
                                            returnModel = box;
                                        }else if (![returnModel isEqual:box])
                                        {
                                            [returnModels safeAddObject:box];
                                        }
                                    }
                                }
                            }else{
                                subItem.isSelected = YES;
                                subItem.parentItem.isSelected = YES;
                                [returnItems safeAddObject:subItem];
                                if (returnModel == nil) {
                                    returnModel = box;
                                }else if (![returnModel isEqual:box])
                                {
                                    [returnModels safeAddObject:box];
                                }
                            }
                        }
                    }
                }
                
                
            }else{
                
                for (MMItem *subsubItem in subItem.childrenNodes) {
                    
                    if (([subsubItem.key isEqualToString:[box key]] && subsubItem.key2 == nil)
                        || ([subsubItem.key2 isEqualToString:[box key2]] && [subsubItem.key isEqualToString:[box key]])) {
                        NSArray *codeArray = [[box code] componentsSeparatedByString:@","];
                        NSArray *code2Array = [[box code2] componentsSeparatedByString:@","];
                        for (NSString *code in codeArray) {
                            if ([code isEqualToString:subsubItem.code]) {
                                if (code2Array.count > 0) {
                                    
                                    for (NSString *code2 in code2Array)
                                    {
                                        if ([code2 isEqualToString:subsubItem.code2])
                                        {
                                            subsubItem.isSelected = YES;
                                            subsubItem.parentItem.isSelected = YES;
                                            [returnItems safeAddObject:subsubItem];
                                            if (returnModel == nil) {
                                                returnModel = box;
                                            }else if (![returnModel isEqual:box])
                                            {
                                                [returnModels safeAddObject:box];
                                            }
                                        }
                                    }
                                }else{
                                    subsubItem.isSelected = YES;
                                    subsubItem.parentItem.isSelected = YES;
                                    [returnItems safeAddObject:subsubItem];
                                    if (returnModel == nil) {
                                        returnModel = box;
                                    }else if (![returnModel isEqual:box])
                                    {
                                        [returnModels safeAddObject:box];
                                    }
                                }
                            }
                        }
                        
                    }
                    
                }
                
            }
        }
    }
    
    if (returnModels.count <= 0) {
        [returnModels safeAddObject:returnModel];
    }
    [mutDic setObject:returnModels forKey:@"boxs"];
    [mutDic setObject:returnItems forKey:@"items"];
    return mutDic;
    
}



/**
 根据boxValues 设置当前筛选项的title，以及当前选中的值
 
 @param boxValues MMComBoxOldValue
 */
- (void)updateValueWithData:(nullable NSArray *)boxValues
{
    [self resetAllChoiceToNone];
    
    NSMutableArray *mutArray = [NSMutableArray arrayWithArray:boxValues];
    NSMutableDictionary *dicSort = [NSMutableDictionary dictionaryWithCapacity:2];
    for (int i = 0;i < mutArray.count;i++) {
        FFBaseKeyCodeModel  *model  = [mutArray safeObjectAtIndex:i];
        if ([model.key isEqualToString:kSortTypeKey] || [model.key isEqualToString:kSortKey]) {
            [dicSort safeSetObject:model.code forKey:model.key];
            [mutArray removeObject:model];
            i--;
        }
    }
    if (dicSort.count > 0) {
        FFBaseKeyCodeModel *model = [FFBaseKeyCodeModel modelWithKey:kSortKey code:dicSort[kSortKey]];
        model.key2 = kSortTypeKey;
        model.code2 = dicSort[kSortTypeKey];
        [mutArray safeAddObject:model];
    }
    
    
#if DEBUG
    // NSLog(@"updateValueWithData:%@",boxValues);
#endif
    for (int i = 0;i < [self.dataSource numberOfColumnsIncomBoBoxView:self]; i++) {
        
        MMItem  *rootItem  =  [self.dataSource comBoBoxView:self infomationForColumn:i];
        NSMutableDictionary *mutDic = [self getSelectItemBoxValue:rootItem boxArray:mutArray];
        if (rootItem.displayType == MMPopupViewDisplayTypeFilters) {
            continue;
        }
        NSArray *items = [mutDic objectForKey:@"items"];
        NSArray *boxs = [mutDic objectForKey:@"boxs"];
        
        NSString *rootTitle = nil;
        FFBaseKeyCodeModel *boxModel = [boxs safeObjectAtIndex:0];
        if (items.count > 0 && boxs.count > 0) {
            
            for (MMItem *item in items) {
                if (rootItem.selectedType == MMPopupViewSingleSelection){
                    rootTitle = item.title;
                    break;
                }
            }
            
        }
        
        if ([rootTitle hasPrefix:@"全部"]) {
            //需求中区域和分类，如果选中以全部为前缀的title 把前缀干掉
            if ([boxModel.key isEqualToString:kCountyIdKey] || [boxModel.key isEqualToString:kCategoryIdKey]) {
                if (rootTitle.length > 2) {
                    rootTitle = [rootTitle stringByReplacingOccurrencesOfString:@"全部" withString:@""];
                }
            }
            
        }
        
        //如果title是全部，需要更改为默认的值
        if ([rootTitle isEqualToString:@"全部"]) {
            
            if ([rootItem.rootTitle isEqualToString:@"分类"]) {
                if ([boxModel choiceDefault]) {
                    rootItem.title = rootItem.rootTitle;
                }else{
                    rootItem.title = rootTitle;
                }
            }else{
                rootItem.title = rootItem.rootTitle;
            }
            
            
        }else{
            rootItem.title = (rootTitle != nil?rootTitle:rootItem.title);
        }
        
        //附近的时候做一些特殊的业务逻辑处理，保留上一次选择的title但是列表中又没有的值，蛋疼的需求
        if (i == 0 && self.oldDistriItem && [rootItem.title isEqualToString:@"附近"]) {
            NSString *key = nil;
            BOOL  hasDis = NO;
            for (FFBaseKeyCodeModel *model in boxValues) {
                if ([model.key isEqualToString:kCountyIdKey] || [model.key isEqualToString:kBusinessDisKey]) {
                    hasDis = YES;
                    key = model.key;
                    break;
                }
            }
            if (hasDis) {
                rootItem.title = self.oldDistriItem.title;
            }
        }
    }
    [self reload];
    
}

/**
 清除所有当前的选择
 */
- (void)cleanAllChoice
{
    [self resetAllChoiceToNone];
    [self.popupView updateSelectPath];
    [self reload];
}



/**
 把所有的选择置为NO
 */
- (void)resetAllChoiceToNone
{
    NSUInteger count = 0;
    if ([self.dataSource respondsToSelector:@selector(numberOfColumnsIncomBoBoxView:)]) {
        count = [self.dataSource numberOfColumnsIncomBoBoxView:self];
    }
    if ([self.dataSource respondsToSelector:@selector(comBoBoxView:infomationForColumn:)]) {
        for (NSUInteger i = 0; i < count; i ++) {
            MMItem *rootItem = [self.dataSource comBoBoxView:self infomationForColumn:i];
            for (MMItem *subItem in rootItem.childrenNodes) {
                if (subItem.childrenNodes.count) {
                    for (MMItem *subsubItem in subItem.childrenNodes) {
                        subsubItem.isSelected = NO;
                        subItem.isSelected = NO;
                    }
                }else{
                    subItem.isSelected = NO;
                }
            }
            rootItem.title = rootItem.rootTitle;
        }
    }
}




/**
 重新刷新数据
 */
- (void)reload {
    
    for (UIView  *subView in self.topBgView.subviews) {
        if (subView != self.topBgView) {
            [subView removeFromSuperview];
        }
    }
    [self.dropDownBoxArray removeAllObjects];
    [self.itemArray removeAllObjects];
    
    NSUInteger count = 0;
    if ([self.dataSource respondsToSelector:@selector(numberOfColumnsIncomBoBoxView:)]) {
        count = [self.dataSource numberOfColumnsIncomBoBoxView:self];
    }
    CGFloat width = (self.ff_width-30)/count;
    if ([self.dataSource respondsToSelector:@selector(comBoBoxView:infomationForColumn:)]) {
        for (NSUInteger i = 0; i < count; i ++) {
            MMItem *item = [self.dataSource comBoBoxView:self infomationForColumn:i];
            [item findTheTypeOfPopUpView];
            MMDropDownBox *dropBox = [[MMDropDownBox alloc] initWithFrame:CGRectMake(i*width,0, width, self.ff_height-self.topSpaceMargin-self.bottomSpaceMargin) titleName:item.title withIcon:item.iconType];
            [dropBox updateTitleContent:item.title];
            dropBox.tag = i;
            dropBox.delegate = self;
            [dropBox setLineHide:YES];
            [self.topBgView addSubview:dropBox];
            [self.dropDownBoxArray addObject:dropBox];
            [self.itemArray addObject:item];
            if (i == count-1) {
                [dropBox setDotHide:YES];
            }
        }
    }
}



/**
 隐藏当前popupView
 */
- (void)dimissPopView {
    
    for (MMDropDownBox *box in self.dropDownBoxArray) {
        [box setLineHide:YES];
    }
    
    if (self.popupView.superview) {
        
        [self.popupView dismissWithCompletion:nil];
    }
    
}


/**
 当前popupView是否是显示状态

 @return 是否是显示状态
 */
- (BOOL)isPopviewShow
{
    if (self.popupView.superview) {
        return YES;
    }
    return NO;
}
#pragma mark - Private Method


/**
 显示index位置的popupView

 @param index 位置
 */
- (void)showNewPopupView:(NSUInteger)index
{
    for (MMBasePopupView *popView in self.subviews) {
        if ([popView isKindOfClass:[MMBasePopupView class]]) {
            [popView dismissWithCompletion:nil];
        }
    }
    
    self.isAnimation = YES;
    MMItem *item = [self.itemArray safeObjectAtIndex:index];
    MMBasePopupView *popupView = [MMBasePopupView getSubPopupView:item];
    [popupView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(emptyAction:)]];
    popupView.delegate = self;
    popupView.tag = index;
    self.popupView = popupView;
    [popupView updateSelectPath];
    [self.symbolArray addObject:popupView];
    
    for (int i = 0; i <self.dropDownBoxArray.count; i++) {
        MMDropDownBox *currentBox  = [self.dropDownBoxArray safeObjectAtIndex:i];
        [currentBox updateTitleState:(i == index)];
        [currentBox setLineHide:(i != index)];
    }
    
    //    if ([self.delegate respondsToSelector:@selector(comBoBoxView:actionType:atIndex:)]) {
    //        [self.delegate comBoBoxView:self actionType:MMComBoBoxViewShowActionTypePop atIndex:index];
    //    }
    
    [self.superview bringSubviewToFront:self];
    dispatch_async(dispatch_get_main_queue(), ^{
        [popupView popupViewFromView:self completion:^ {
            self.isAnimation = NO;
            
            if ([self.delegate respondsToSelector:@selector(comBoBoxView:actionType:atIndex:)]) {
                [self.delegate comBoBoxView:self actionType:MMComBoBoxViewShowActionTypePop atIndex:index];
            }
            
        }];
    });
    
}


#pragma mark
#pragma mark 截取事件的方法
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *view = [super hitTest:point withEvent:event];
    if (view == nil && self.popupView.superview) {
        CGPoint tp = [self convertPoint:point fromView:self];
        if (CGRectContainsPoint(self.popupView.frame, tp)) {
            view = self;
        }
        CGRect frame = [self convertRect:self.popupView.shadowView.frame toView:self];
        if (CGRectContainsPoint(frame, tp)) {
            return self;
        }
    }
    return view;
}
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint tp = [self convertPoint:point fromView:self];
    
    if (self.popupView.superview) {
        
        if (CGRectContainsPoint(self.popupView.frame, tp)) {
            return YES;
        }
        CGRect frame = [self convertRect:self.popupView.shadowView.frame toView:self];
        if (CGRectContainsPoint(frame, tp)) {
            return YES;
        }
    }
    return [super pointInside:point withEvent:event];
}



/**
 根据选中的selectPaths 返回MMItem数组
 
 @param selectPaths 选中的selectPaths
 @param rootItem rootItem
 @return 返回的MMItem
 */
- (nullable NSArray*)getModelsBySelectePath:(nullable NSArray*)selectPaths rootItem:(nullable MMItem*)rootItem
{
    NSMutableArray  *array = [NSMutableArray array];
    for (MMSelectedPath *path in selectPaths) {
        MMItem *subItem = [rootItem findItemBySelectedPath:path];
        [array safeAddObject:subItem];
    }
    return [NSArray arrayWithArray:array];
}

#pragma mark - MMDropDownBoxDelegate
- (void)didTapDropDownBox:(nullable MMDropDownBox *)dropDownBox atIndex:(NSUInteger)index {
    
    if (self.isAnimation == YES) return;
    
    //点击后先判断symbolArray有没有标示
    if (self.symbolArray.count > 0) {
        
        MMBasePopupView * lastView = [self.symbolArray firstObject];
        MMItem *rootItem = lastView.item;
        
        [self.symbolArray removeAllObjects];
        for (MMDropDownBox *box in self.dropDownBoxArray) {
            [box setLineHide:YES];
        }
        //如果是点击当前的，那么就不需要再显示
        if (rootItem == [self.itemArray safeObjectAtIndex:index]) {
            [lastView dismissWithCompletion:^{
                
            }];
            return;
        }
        [lastView dismissWithCompletion:^{
            [self showNewPopupView:index];
        }];
        
    }else{
        [self showNewPopupView:index];
    }
    
}

#pragma mark - MMPopupViewDelegate
- (void)popupView:(nullable MMBasePopupView *)popupView didSelectedItemsPackagingInArray:(nullable NSArray *)array atIndex:(NSUInteger)index {
    MMItem *item = self.itemArray[index];
    if (item.displayType != MMPopupViewDisplayTypeFilters) {
        //拼接选择项
        NSString  *tempKey = nil;
        NSMutableString *title = [NSMutableString string];
        for (int i = 0; i <array.count; i++) {
            if (title.length > 0) {
                break;
            }
            MMSelectedPath *path = array[i];
            MMItem *tempItem = [item findItemBySelectedPath:path];
            [title setString:tempItem.title];
            tempKey = tempItem.key;
            if ([tempItem.key isEqualToString:kCountyIdKey] ||
                [tempItem.key isEqualToString:kCategoryIdKey]) {
                if ([title hasPrefix:@"全部"] && title.length > 2) {
                    [title setString:[title stringByReplacingOccurrencesOfString:@"全部" withString:@""]];
                }
            }
            
            
            if ([tempItem.key isEqualToString:kCountyIdKey] || [tempItem.key isEqualToString:kBusinessDisKey]) {
                self.oldDistriItem = item;
            }
            
        }
        MMDropDownBox *box = [self.dropDownBoxArray safeObjectAtIndex:index];
        
        //如果title是全部，需要更改为默认的值
        if ([title isEqualToString:@"全部"]) {
            [box updateTitleContent:item.rootTitle];
        }else if (title.length > 0){
            [box updateTitleContent:title];
        }else{
            [box updateTitleContent:item.rootTitle];
        }
        
    }; //筛选不做UI赋值操作 直接将item的路径回调回去就好了
    
    
    if ([self.delegate respondsToSelector:@selector(comBoBoxView:didSelectedItemsPackagingInArray:atIndex:)]) {
        [self.delegate comBoBoxView:self didSelectedItemsPackagingInArray:array atIndex:index];
    }
    
    if ([self.delegate respondsToSelector:@selector(comBoBoxView:didSelectedModelsInArray:atIndex:)]) {
        [self.delegate comBoBoxView:self didSelectedModelsInArray:[self getModelsBySelectePath:array rootItem:item] atIndex:index];
    }
}

- (void)popupViewWillDismiss:(nullable MMBasePopupView *)popupView {
    
    [self.symbolArray removeAllObjects];
    for (MMDropDownBox *currentBox in self.dropDownBoxArray) {
        [currentBox updateTitleState:NO];
    }
    for (MMDropDownBox *box in self.dropDownBoxArray) {
        [box setLineHide:YES];
    }
}


/**
 点击重置按钮的回调
 
 @param popupView MMBasePopupView
 @param index tag
 */
- (void)popupViewDidResetValue:(nullable MMBasePopupView *)popupView atIndex:(NSUInteger)index
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(popupViewDidResetValue:atIndex:)]) {
        [self.delegate popupViewDidResetValue:popupView.item atIndex:index];
    }
}

- (void)popupViewDidDismiss:(nullable MMBasePopupView *)popupView {
    
    if ([self.delegate respondsToSelector:@selector(comBoBoxView:actionType:atIndex:)]) {
        [self.delegate comBoBoxView:self actionType:MMComBoBoxViewShowActionTypePackUp atIndex:0];
    }
}

@end
