//
//  main.m
//  MMCombobox
//
//  Created by ranjingfu on 04/06/2017.
//  Copyright (c) 2017 ranjingfu. All rights reserved.
//

@import UIKit;
#import "MMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MMAppDelegate class]));
    }
}
