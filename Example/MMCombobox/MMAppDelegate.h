//
//  MMAppDelegate.h
//  MMCombobox
//
//  Created by ranjingfu on 04/06/2017.
//  Copyright (c) 2017 ranjingfu. All rights reserved.
//

@import UIKit;

@interface MMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
