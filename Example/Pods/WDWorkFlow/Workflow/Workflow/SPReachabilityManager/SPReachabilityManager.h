#import <Foundation/Foundation.h>

typedef enum : NSInteger {
    kSPConnectionStatusUnknown = -1,
    kSPConnectionStatusNone = 0,
    kSPConnectionStatusWifi,
    kSPConnectionStatusInternet,
}SPConnectionStatus;

extern NSString *kSPConnectionStatusCurrentValueNotification;
extern NSString *kSPConnectionStatusChangedNotification;

@interface SPReachabilityManager : NSObject

@property (nonatomic, readonly) SPConnectionStatus connStatus;

+ (SPReachabilityManager *)sharedInstance;

- (void)startListeningConnectionStatus;
- (void)refreshConnectionStatus;

@end


