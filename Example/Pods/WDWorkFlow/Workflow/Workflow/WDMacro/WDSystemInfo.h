//
//  WDSystemInfo.h
//  Pods
//
//  Created by 陆锋平 on 2017/2/22.
//
//

#ifndef WDSystemInfo_h
#define WDSystemInfo_h

//autoLayout
#define kWDAutoresizingRB   UIViewAutoresizingFlexibleRightMargin |\
UIViewAutoresizingFlexibleBottomMargin


#define kWDAutoresizingRBW  UIViewAutoresizingFlexibleRightMargin |\
UIViewAutoresizingFlexibleBottomMargin|\
UIViewAutoresizingFlexibleWidth


#define kWDAutoresizingRTW  UIViewAutoresizingFlexibleRightMargin |\
UIViewAutoresizingFlexibleTopMargin   |\
UIViewAutoresizingFlexibleWidth

#define kWDAutoresizingWH   UIViewAutoresizingFlexibleWidth|\
UIViewAutoresizingFlexibleHeight

//颜色

#define RGBColor(r, g, b) RGBAColor(r, g, b, 255)
#define RGBAColor(r, g, b, a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a/255.0f]  //a -> 0 ~ 255

#define RGBCodeColor(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1]
#define RGBCodeAlphaColor(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a] // a -> 0 ~ 1


// 系统版本和屏幕判断
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)

#define IOS_2_0 @"2.0"
#define IOS_3_0 @"3.0"
#define IOS_4_0 @"4.0"
#define IOS_5_0 @"5.0"
#define IOS_6_0 @"6.0"
#define IOS_7_0 @"7.0"
#define IOS_8_0 @"8.0"
#define IOS_9_0 @"9.0"
#define IOS_10_0 @"10.0"

#define IOSVersion      [[UIDevice currentDevice].systemVersion floatValue]

#define IOS10_OR_LATER       ( [[[UIDevice currentDevice] systemVersion] compare:IOS_10_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS9_OR_LATER       ( [[[UIDevice currentDevice] systemVersion] compare:IOS_9_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS8_OR_LATER		( [[[UIDevice currentDevice] systemVersion] compare:IOS_8_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS7_OR_LATER		( [[[UIDevice currentDevice] systemVersion] compare:IOS_7_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS6_OR_LATER		( [[[UIDevice currentDevice] systemVersion] compare:IOS_6_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS5_OR_LATER		( [[[UIDevice currentDevice] systemVersion] compare:IOS_5_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS4_OR_LATER		( [[[UIDevice currentDevice] systemVersion] compare:IOS_4_0 options:NSNumericSearch] != NSOrderedAscending )
#define IOS3_OR_LATER		( [[[UIDevice currentDevice] systemVersion] compare:IOS_3_0 options:NSNumericSearch] != NSOrderedAscending )

#define IOS8_EARLIER		( !IOS8_OR_LATER )
#define IOS7_EARLIER		( !IOS7_OR_LATER )
#define IOS6_EARLIER		( !IOS6_OR_LATER )
#define IOS5_EARLIER		( !IOS5_OR_LATER )
#define IOS4_EARLIER		( !IOS4_OR_LATER )

// >=
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
// >
#define SYSTEM_VERSION_GREATER_THAN(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
// ==
#define SYSTEM_VERSION_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

#define IS_SCREEN_55_INCH	([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
#define IS_SCREEN_47_INCH	([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define IS_SCREEN_4_INCH	([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define IS_SCREEN_35_INCH	([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)


#else	// #if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)

#define IOS7_OR_LATER		(NO)
#define IOS6_OR_LATER		(NO)
#define IOS5_OR_LATER		(NO)
#define IOS4_OR_LATER		(NO)
#define IOS3_OR_LATER		(NO)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) (NO)
#define SYSTEM_VERSION_GREATER_THAN(v) (NO)
#define SYSTEM_VERSION_EQUAL_TO(v) (NO)

#define IS_SCREEN_4_INCH	(NO)
#define IS_SCREEN_35_INCH	(NO)

#endif

#endif /* WDSystemInfo_h */
