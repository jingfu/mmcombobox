//
//  WDGuideDog.h
//  Pods
//
//  Created by 刘彬 on 2017/3/20.
//
//

#import <Foundation/Foundation.h>


/**
 App引导功能管理
 */
@interface WDGuideDog : NSObject

/*!
 *  @brief 执行app引导功能，以版本号为判断标准
 *
 *  @param guideKey         引导功能key
 *  @param ignoreVersion    是否忽略版本号(忽略版本号则一个key只执行一次，否则每次版本更新都执行一次)
 *  @param guideBlock       将要执行的引导块
 *  @param unguideBlock     不需要执行引导块的时候执行的引导块
 *
 */
+ (void)wd_guideWithKey:(NSString *)guideKey
       ignoreAppVersion:(BOOL)ignoreVersion
             guideBlock:(dispatch_block_t)guideBlock
           unguideBlock:(dispatch_block_t)unguideBlock;

/*!
 *  @brief 执行app引导功能，自定义判断标准
 *
 *  @param guideKey         引导功能key
 *  @param compareValue     需要比较的velue
 *  @param guideBlock       将要执行的引导块
 *  @param unguideBlock     不需要执行引导块的时候执行的引导块
 *
 */
+ (void)wd_guideWithKey:(NSString *)guideKey
       compareWithValue:(NSString *)compareValue
             guideBlock:(dispatch_block_t)guideBlock
           unguideBlock:(dispatch_block_t)unguideBlock;

/*!
 *  @brief 重置引导功能key，用于测试
 *  @param guideKey         引导功能key
 */
+ (void)wd_resetWithKey:(NSString *)guideKey;

@end
