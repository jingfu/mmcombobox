//
//  WDGuideDog.m
//  Pods
//
//  Created by 刘彬 on 2017/3/20.
//
//

#import "WDGuideDog.h"
#import "NSString+Safe.h"

@implementation WDGuideDog

+ (void)wd_guideWithKey:(NSString *)guideKey
       ignoreAppVersion:(BOOL)ignoreVersion
             guideBlock:(dispatch_block_t)guideBlock
           unguideBlock:(dispatch_block_t)unguideBlock{
    NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
    // key对应的版本号
    NSString *lastVersion = [NSString safeStringFromObject:[standard objectForKey:guideKey]];
    // 当前版本号
    NSString *appVersion = [NSString safeStringFromObject:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    // 判断是否需要执行引导功能
    BOOL needGuide = YES;
    if (ignoreVersion) {
        needGuide = lastVersion == nil || lastVersion.length == 0;
    } else{
        needGuide = ![appVersion isEqualToString:lastVersion];
    }
    
    if (needGuide) {
        if (guideBlock != nil) {
            guideBlock();
        }
        [standard setObject:appVersion forKey:guideKey];
        [standard synchronize];
    }else{
        if (unguideBlock != nil) {
            unguideBlock();
        }
    }
}

+ (void)wd_guideWithKey:(NSString *)guideKey
       compareWithValue:(NSString *)compareValue
             guideBlock:(dispatch_block_t)guideBlock
           unguideBlock:(dispatch_block_t)unguideBlock{
    NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
    // key对应的版本号
    NSString *currentValue = [NSString safeStringFromObject:[standard objectForKey:guideKey]];
    // 判断是否需要执行引导功能
    BOOL needGuide = currentValue == nil || currentValue.length == 0 || ![currentValue isEqualToString:compareValue];
    if (needGuide) {
        if (guideBlock != nil) {
            guideBlock();
        }
        [standard setObject:compareValue forKey:guideKey];
        [standard synchronize];
    }else{
        if (unguideBlock != nil) {
            unguideBlock();
        }
    }
}

+ (void)wd_resetWithKey:(NSString *)guideKey{
    NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
    [standard removeObjectForKey:guideKey];
    [standard synchronize];
}

@end
