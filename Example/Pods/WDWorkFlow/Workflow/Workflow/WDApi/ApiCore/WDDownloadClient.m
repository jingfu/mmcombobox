//
//  WDDownloadClient.m
//  FeiFan
//
//  Created by fy on 16/1/13.
//  Copyright © 2016年 Wanda Inc. All rights reserved.
//

#import "WDDownloadClient.h"
#import "AFURLSessionManager.h"
#import <libkern/OSAtomic.h>
#import "FFWeakObj.h"
#import "SafeCategory.h"


@interface WDDownloadClient ()

@property (nonatomic, strong) NSMutableDictionary *downloadingApis;


@end

@implementation WDDownloadClient
FF_DEF_SINGLETON

static OSSpinLock _downloadSpinLock = OS_SPINLOCK_INIT;

- (instancetype)init
{
    if (self = [super init]) {
        self.downloadingApis = [NSMutableDictionary dictionary];
    }
    
    return self ;
}

- (void)execute:(WDDownloadApi *)api
        success:(downloadSuccess)success
        failure:(downloadFailure)failure
{
    
    if (![api resourceUrl]) {
        return;
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@?file=%@&suffix=%@",[api baseUrl],[api relativePath],[api resourceUrl],[api fileType]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    // 服务端不关心 contentType，因此客户端不做验证
    ((AFJSONResponseSerializer *)manager.responseSerializer).acceptableContentTypes = nil;

    NSURL *URL = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    if (![fm fileExistsAtPath:[api toDir]]) {
        [fm createDirectoryAtPath:[api toDir] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    weakObj(self);
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSString *filePath;
        
        if ([api fileName]) {
            
            filePath = [[[api toDir] mutableCopy] stringByAppendingPathComponent:[api fileName]];
            
        }
        else
        {
            filePath = [[[api toDir] mutableCopy] stringByAppendingPathComponent:response.suggestedFilename];
            
        }
        
        return [NSURL fileURLWithPath:filePath];

    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        
        strongObj(self);
        if (error) {
            failure(error);
        }
        else{
            success(response, filePath);
        }
        
        OSSpinLockLock(&_downloadSpinLock);
        [self.downloadingApis removeObjectForKey:urlStr];
        OSSpinLockUnlock(&_downloadSpinLock);
    }];
    
    OSSpinLockLock(&_downloadSpinLock);
    [self.downloadingApis safeSetObject:api forKey:urlStr];
    OSSpinLockUnlock(&_downloadSpinLock);
    
    [downloadTask resume];
}

@end
