//
//  WDFileDownloadOperation.h
//  Pods
//
//  Created by 刘彬 on 2017/3/22.
//
//

#import <Foundation/Foundation.h>
#import "WDFileDownloader.h"

// 开始下载通知
FOUNDATION_EXTERN NSString *const WDStartDownloadNotification;
// 停止下载通知，包括下载成功，下载失败，取消下载
FOUNDATION_EXTERN NSString *const WDStopDownloadNotification;
// 完成下载通知，只有下载成功才会发送通知
FOUNDATION_EXTERN NSString *const WDFinishDownloadNotification;

// NSURL KEY
FOUNDATION_EXTERN NSString *const WDDownloadURLKey;
FOUNDATION_EXTERN NSString *const WDDownloadStatusCodeKey;

@interface WDFileDownloadOperation : NSOperation <WDFileDownloaderCancelable,NSURLSessionTaskDelegate,NSURLSessionDataDelegate>

@property (nonatomic, strong, readonly) NSURLRequest *request;
@property (nonatomic, strong, readonly) NSURLSessionTask *dataTask;
// 对否允许后台继续下载，默认NO
@property (nonatomic, assign) BOOL continueDownloadInBackground;
// 重试次数，默认0
@property (nonatomic, assign) NSInteger retryCount;

/*!
 *  @brief 创建一个operation
 *
 *  @param request        http请求
 *  @param session        session
 *  @param progressBlock  过程回调
 *  @param completedBlock 完成回调
 *  @param cancelBlock    取消回调
 *
 *  @return 返回实例
 */
- (instancetype)initWithRequest:(NSURLRequest *)request
                      inSession:(NSURLSession *)session
                       progress:(WDFileDownloaderProgressBlock)progressBlock
                      completed:(WDFileDownloaderCompletedBlock)completedBlock
                      cancelled:(WDFileDownloaderCancelBlock)cancelBlock;

@end
