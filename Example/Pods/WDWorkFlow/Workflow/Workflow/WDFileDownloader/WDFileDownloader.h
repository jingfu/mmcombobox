//
//  WDFileDownloader.h
//  Pods
//
//  Created by 刘彬 on 2017/3/22.
//
//

#import <Foundation/Foundation.h>


/*!
 *  @brief 下载过程回调
 *
 *  @param receivedSize 收到的大小
 *  @param expectedSize 最终文件大小
 */
typedef void(^WDFileDownloaderProgressBlock)(NSUInteger receivedSize, NSUInteger expectedSize);
/*!
 *  @brief 下载完成回调
 *
 *  @param data     下载的数据
 *  @param error    错误
 *  @param finished 是否结束
 */
typedef void(^WDFileDownloaderCompletedBlock)(NSData *data, NSError *error, BOOL finished);

/*!
 *  @brief 取消下载回调
 */
typedef void(^WDFileDownloaderCancelBlock)(void);

@protocol WDFileDownloaderCancelable <NSObject>

- (void)cancel;

@end

@interface WDFileDownloader : NSObject

/**
 *  最大下载并发数， 默认为3
 */
@property (nonatomic, assign) NSInteger maxConcurrentDownloads;

/**
 *  当前下载数
 */
@property (nonatomic, assign, readonly) NSUInteger currentDownloadCount;

/**
 *  下载超时时间 默认为 30s
 */
@property (nonatomic, assign) NSTimeInterval downloadTimeout;

/**
 *  失败时的重试次数，默认0
 */
@property (nonatomic, assign) NSInteger retryCount;

+(WDFileDownloader *)downloader;

/*！
 *  @brief 异步下载文件
 *
 *  @param url            文件资源url
 *  @param progressBlock  进度回调
 *  @param completedBlock 完成回调
 *
 *  @return 一个可取消的operation，如WDFileDownloadOperation
 */
- (id<WDFileDownloaderCancelable>)downloadFileWithURL:(NSURL *)url
                                             progress:(WDFileDownloaderProgressBlock)progressBlock
                                            completed:(WDFileDownloaderCompletedBlock)completedBlock;

/*！
 *  @brief 同步下载文件,利用嵌套runloop实现
 *
 *  @param url           文件资源url
 *  @param progressBlock 进度回调
 *
 *  @return 返回下载的文件
 */
- (NSData *)downloadFileUntilFinishedWithURL:(NSURL *)url progress:(WDFileDownloaderProgressBlock)progressBlock;

/*!
 *  @brief 设置暂停和恢复队列
 *
 *  @param suspended YES-暂停 NO-恢复
 */
- (void)setSuspended:(BOOL)suspended;

/*!
 *  @brief 取消队列的所有操作
 */
- (void)cancelAllDownloads;


/*！
 *  @brief 设置header字段，主要用于配置accept字段
 *  accept字段设置请参考 http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
 *
 *  @param value header值
 *  @param field accept字段
 */
- (void)setValue:(NSString *)value forHTTPHeaderField:(NSString *)field;

/*！
 * 返回header头字段
 *
 * @return 参会header中对应的字段值
 */
- (NSString *)valueForHTTPHeaderField:(NSString *)field;

@end
