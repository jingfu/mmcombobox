//
//  NSThread+Runloop.h
//  Pods
//
//  Created by 刘彬 on 2017/3/22.
//
//

#import <Foundation/Foundation.h>

typedef BOOL WDRunloopBreak;

@interface NSThread (Runloop)

/*!
 *  @brief 防止当前runloop没有loopsource,手动添加
 */
- (void)wd_addLoopSource;

/*！
 *  @brief 采用嵌套的runloop阻塞当前线程，直到某个条件出现
 *
 *  @param condition 条件，返回yes表示跳出block
 *  @param time      超时时间，若time小于等于零，表示永不超时
 */
+ (void)wd_runloopBlockUntilCondition:(WDRunloopBreak (^)(void))condition timeOut:(NSTimeInterval)time;

@end
