//
//  NSFileManager+WDHelper.h
//  Pods
//
//  Created by 刘彬 on 2017/3/24.
//
//

#import <Foundation/Foundation.h>

NSString *WDDocumentsFolder();
NSString *WDCacheFolder();
NSString *WDLibraryFolder();
NSString *WDBundleFolder();

@interface NSFileManager (WDHelper)

/*！
 *  @brief  创建一个实例，最好不要用defaultManager，它不是线程安全的
 *
 *  @return 返回一个临时实例
 */
+ (instancetype)wd_fileManager;

/*！
 *  @brief  获取Document中对应的目录，没有则创建
 *
 *  @param  pathComponents 要追加的目录
 *  @return 创建出的路径，创建失败返回nil
 */
- (NSString *)wd_pathDocumentDirectoryWithPathComponents:(NSString *)pathComponents;

/*！
 *  @brief  获取Library/Caches中对应的目录，没有则创建
 *
 *  @param  pathComponents 要追加的目录
 *  @return 创建出的路径，创建失败返回nil
 */
- (NSString *)wd_pathCachesDirectoryWithPathComponents:(NSString *)pathComponents;

/*！
 *  @brief  获取Temp中对应的目录，没有则创建
 *
 *  @param  pathComponents 要追加的目录
 *  @return 创建出的路径，创建失败返回nil
 */
- (NSString *)wd_pathTemporaryDirectoryWithPathComponents:(NSString *)pathComponents;

/*！
 *  @brief  禁止文件备份到iCloud
 *
 *  @param  path 文件路径
 *  @return BOOL
 */
+ (BOOL)wd_addSkipBackupAttributeWithPath:(NSString *)path;

/*！
 *  @brief  禁止文件备份到iCloud
 *
 *  @param  URL 文件路径
 *  @return BOOL
 */
+ (BOOL)wd_addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

/*！
 *  @brief  返回一个操作文件的文件串行队列
 *
 *  @param  URL 文件路径
 *  @return BOOL
 */
+ (dispatch_queue_t)wd_fileIOSerialQueue;
@end
