//
//  NSFileManager+WDHelper.m
//  Pods
//
//  Created by 刘彬 on 2017/3/24.
//
//

#import "NSFileManager+WDHelper.h"

NSString *WDDocumentsFolder()
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}

NSString *WDCacheFolder()
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
}

NSString *WDLibraryFolder()
{
    return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
}

NSString *WDBundleFolder()
{
    return [[NSBundle mainBundle] bundlePath];
}

@implementation NSFileManager (WDHelper)

+ (instancetype)wd_fileManager {
    return [[NSFileManager alloc] init];
}

- (NSString *)wd_pathDocumentDirectoryWithPathComponents:(NSString *)pathComponents
{
    static NSString *documentsDirectory = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        @autoreleasepool {
            documentsDirectory = WDDocumentsFolder();
        }
    });
    
    if (!pathComponents) {
        return nil;
    }
    
    NSString *path = [documentsDirectory stringByAppendingPathComponent:pathComponents];
    
    @autoreleasepool {
        NSError *error = nil;
        if (![self fileExistsAtPath:path]) {
            [self createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        if (error) {
            return nil;
        }
    }
    return path;
}

- (NSString *)wd_pathCachesDirectoryWithPathComponents:(NSString *)pathComponents {
    static NSString *cachesDirectory = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        @autoreleasepool {
            cachesDirectory = WDCacheFolder();
        }
    });
    
    if (!pathComponents) {
        return nil;
    }
    
    NSString *path = [cachesDirectory stringByAppendingPathComponent:pathComponents];
    
    @autoreleasepool {
        NSError *error = nil;
        if (![self fileExistsAtPath:path]) {
            [self createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        if (error) {
            return nil;
        }
    }
    return path;
}

- (NSString *)wd_pathTemporaryDirectoryWithPathComponents:(NSString *)pathComponents {
    if (!pathComponents) {
        return nil;
    }
    
    NSString *tmpDir =  NSTemporaryDirectory();
    NSString *path = [tmpDir stringByAppendingPathComponent:pathComponents];
    
    @autoreleasepool {
        NSError *error = nil;
        if (![self fileExistsAtPath:path]) {
            [self createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        if (error) {
            return nil;
        }
    }
    return path;
}

#pragma mark - 文件属性操作
+ (BOOL)wd_addSkipBackupAttributeWithPath:(NSString *)path {
    if (path.length == 0) {
        return NO;
    }
    
    NSURL *url= [NSURL fileURLWithPath:path];
    if([[NSFileManager wd_fileManager] fileExistsAtPath: [url path]]){
        return [self wd_addSkipBackupAttributeToItemAtURL:url];
    } else{
        return NO;
    }
}

+ (BOOL)wd_addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    if ( URL == nil ) {
        return NO;
    }
    
    if (![[NSFileManager wd_fileManager] fileExistsAtPath: [URL path]]) {
        return NO;
    }
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+ (dispatch_queue_t)wd_fileIOSerialQueue {
    static dispatch_queue_t queue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue = dispatch_queue_create("com.wandan.fio.serial.queue", DISPATCH_QUEUE_SERIAL);
    });
    return queue;
}

@end
