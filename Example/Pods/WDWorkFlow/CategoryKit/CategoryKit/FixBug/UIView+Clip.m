//
//  UIView+Clip.m
//  FeiFan
//
//  Created by yulm on 2017/1/10.
//  Copyright © 2017年 Wanda Inc. All rights reserved.
//

#import "UIView+Clip.h"

@implementation UIView (Clip)

- (void)clipWithCorners:(UIRectCorner)corners cornerRadii:(CGSize)size{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:size];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

@end
