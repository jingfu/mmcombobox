//
//  UIView+Clip.h
//  FeiFan
//
//  Created by yulm on 2017/1/10.
//  Copyright © 2017年 Wanda Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Clip)

- (void)clipWithCorners:(UIRectCorner)corners cornerRadii:(CGSize)size;

@end
